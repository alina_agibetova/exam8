export const CATEGORIES = [
  {nameCategory: 'All', id: 'All'},
  {nameCategory: 'star-wars', id: 'star-wars'},
  {nameCategory: 'famous people', id: 'famous-people'},
  {nameCategory: 'saying', id: 'saying'},
  {nameCategory: 'humour', id: 'humour'},
  {nameCategory: 'motivational', id: 'motivational'},
];
