import { Component, OnInit } from '@angular/core';
import { CATEGORIES } from '../shared/categories';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  categories = CATEGORIES;

  constructor() { }

  ngOnInit(): void {}
}


