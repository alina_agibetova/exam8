import { Component, OnInit } from '@angular/core';
import { CATEGORIES } from '../shared/categories';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Quote } from '../shared/quote.modal';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit {
  categories = CATEGORIES;
  quote!: Quote;
  category = '';
  author = '';
  text = '';
  newId = '';

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const newId = params['id'];
      this.http.get<Quote>(`https://alina-beaf9-default-rtdb.firebaseio.com/quotes/${newId}.json`)
        .subscribe(result => {
          this.quote = result;
          this.author = this.quote.author;
          this.category = this.quote.category;
          this.text = this.quote.text;
        })
    });
  }


  editQuote(){
    if (this.newId){
      const author = this.author;
      const category = this.category;
      const text = this.text;
      const body = {author, category, text};
      this.http.put<Quote>(`https://alina-beaf9-default-rtdb.firebaseio.com/quotes/${this.newId}.json`, body).subscribe();
      void this.router.navigate(['/']);
    } else {
      const author = this.author;
      const category = this.category;
      const text = this.text;
      const body = {author, category, text};
      this.http.post('https://alina-beaf9-default-rtdb.firebaseio.com/quotes.json', body).subscribe();
      void this.router.navigate(['/']);
    }
  }

}
