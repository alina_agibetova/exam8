import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { SidebarComponent } from './sidebar/sidebar.component';

const routes: Routes = [
  {path: '', component: HomeComponent,
    children: [
      {path: 'quote/:id', component: SidebarComponent},
      {path: 'quote/:id/edit', component: NewQuoteComponent},
    ]},

  {path: 'new', component: NewQuoteComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
