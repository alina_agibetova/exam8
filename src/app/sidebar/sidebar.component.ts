import { Component, OnInit } from '@angular/core';
import { Quote } from '../shared/quote.modal';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  quotes!: Quote[];

  constructor(private http: HttpClient, private route: ActivatedRoute) { }


  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const quoteId = params['id'];
      if (quoteId === undefined || quoteId === 'All') {
        this.http.get<{ [id: string]: Quote }>('https://alina-beaf9-default-rtdb.firebaseio.com/quotes.json')
          .pipe(map(result => {
            if (result === null) {
              return [];
            }
            return Object.keys(result).map(id => {
              const quoteData = result[id];
              console.log(result[id]);

              return new Quote(id, quoteData.author, quoteData.category, quoteData.text)
            })
          }))
          .subscribe(quotes => {
            this.quotes = [];
            this.quotes = quotes;
          });
      } else {
        this.http.get<{[id: string]: Quote }>(`https://alina-beaf9-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="${quoteId}"`)
          .pipe(map(result => {
            if (result === null) {
              return [];
            }
            return Object.keys(result).map(id => {
              const quoteData = result[id];
              console.log(result[id]);

              return new Quote(id, quoteData.author, quoteData.category, quoteData.text)
            })
          }))
          .subscribe(quotes => {
            this.quotes = [];
            this.quotes = quotes;
          });
      }
    })}

  deleteQuote() {
    this.route.params.subscribe((params: Params) => {
      const quoteId = params['id'];
      this.http.delete(`https://alina-beaf9-default-rtdb.firebaseio.com/quotes/${quoteId}.json`);
    });
  }
}

